#!/usr/bin/env python

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import unittest

from mj_computer_vision.feature_tracker import FeatureTracker, Feature
import cv2
import numpy as np


class TestFeatureDetector(unittest.TestCase):
    def setUp(self):
        self.detector = FeatureTracker()

        image1 = cv2.imread("test_data/test_image1.jpg")
        self.test_image1 = cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY)

        image2 = cv2.imread("test_data/test_image2.jpg")
        self.test_image2 = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)

    def test_feature_extraction(self):
        self.detector.add_image(self.test_image1)
        feature_count = self.get_feature_count()
        self.assertNotEqual(0, feature_count)

        self.detector.add_image(self.test_image2)
        feature_count = self.get_feature_count()
        self.assertNotEqual(0, feature_count)

    def test_no_features_to_extract(self):
        empty_image = np.zeros_like(self.test_image1)

        self.detector.add_image(empty_image)
        feature_count = self.get_feature_count()
        self.assertEqual(0, feature_count)

    def test_loosing_all_features(self):
        self.detector.add_image(self.test_image1)
        feature_count = self.get_feature_count()
        self.assertNotEqual(0, feature_count)

        empty_image = np.zeros_like(self.test_image1)
        self.detector.add_image(empty_image)
        feature_count = self.get_feature_count()
        self.assertEqual(0, feature_count)

        self.detector.add_image(self.test_image1)
        feature_count = self.get_feature_count()
        self.assertNotEqual(0, feature_count)

    def get_feature_count(self):
        features = self.detector.get_features()
        return len(features)


class TestFeature(unittest.TestCase):
    def setUp(self):
        self.feature = Feature([1, 1], 1)

    def test_repr_and_str(self):
        length = len(repr(self.feature))
        self.assertNotEqual(0, length)

        length = len(str(self.feature))
        self.assertNotEqual(0, length)


if __name__ == '__main__':
    unittest.main()
