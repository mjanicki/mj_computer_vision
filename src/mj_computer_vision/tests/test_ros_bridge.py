#!/usr/bin/env python

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import unittest
from mj_computer_vision.ros_bridge import feature_to_msg, msg_to_feature,\
    feature_list_to_msg, msg_to_feature_list
from mj_computer_vision.feature_tracker import Feature
from mj_computer_vision.msg import Feature as FeatureMsg, FeatureList

from random import randrange


class TestFeatureConversion(unittest.TestCase):
    def test_feature_to_msg(self):
        feature = self.random_feature_factory()
        msg = feature_to_msg(feature)

        self.assert_msg_equals_feature(msg, feature)

    def test_msg_to_feature(self):
        msg = self.random_feature_msg_factory()
        feature = msg_to_feature(msg)

        self.assert_msg_equals_feature(msg, feature)

    def test_feature_list_to_msg(self):
        features = []
        for i in xrange(10):
            feature = self.random_feature_factory()
            features.append(feature)

        msg = feature_list_to_msg(features)

        for feature, msg in [(features[i], msg.features[i])
                             for i in xrange(len(features))]:
            self.assert_msg_equals_feature(msg, feature)

    def test_msg_to_feature_list(self):
        msg = FeatureList()
        for i in xrange(10):
            feature_msg = self.random_feature_msg_factory()
            msg.features.append(feature_msg)

        features = msg_to_feature_list(msg)
        for i in xrange(len(msg.features)):
            self.assert_msg_equals_feature(msg.features[i], features[i])

    def assert_msg_equals_feature(self, msg, feature):
        self.assertEqual(msg.position.x, feature.position[0])
        self.assertEqual(msg.position.y, feature.position[1])
        self.assertEqual(msg.id, feature.id)

    def random_feature_factory(self):
        x = randrange(2000)
        y = randrange(2000)
        identity = randrange(2**64 - 1)
        return Feature([x, y], identity)

    def random_feature_msg_factory(self):
        msg = FeatureMsg()
        msg.position.x = randrange(2000)
        msg.position.y = randrange(2000)
        msg.id = randrange(2**64 - 1)
        return msg


if __name__ == '__main__':
    unittest.main()
