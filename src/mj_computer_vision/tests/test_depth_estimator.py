#!/usr/bin/env python

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import unittest
from mj_computer_vision.depth_estimator import EnlargmentDepthEstimator

import numpy as np
from mj_computer_vision.feature_tracker import Feature
from mj_utils.ros_messages_bridge import TransformBridge


class TestEnlargmentDepthEstimator(unittest.TestCase):
    def setUp(self):
        self.estimator = EnlargmentDepthEstimator(neighborhood_size=20)
        f1 = Feature([10, 20], 1)
        f2 = Feature([11, 22], 2)
        self.features = [f1, f2]
        self.transform = TransformBridge()
        self.transform.translation[:] = np.random.random(3) * 100

    def test_simple_two_features_motion(self):
        self._add_data_to_estimator()
        self._simulate_camera_motion()
        self._add_data_to_estimator()

        depths = self.estimator.get_depths()
        f1 = self.features[0]
        f2 = self.features[1]
        self.assertTrue((f1.position == depths[0].position).all())
        self.assertTrue((f2.position == depths[1].position).all())
        self.assertEqual(3/4, depths[0].depth)
        self.assertEqual(3/4, depths[1].depth)

    def test_no_motion(self):
        self._add_data_to_estimator()
        self._add_data_to_estimator()

        depths = self.estimator.get_depths()
        self.assertEqual(0, len(depths))

    def test_no_position_change(self):
        self._add_data_to_estimator()

        translation = self.transform.translation.copy()
        self._simulate_camera_motion()
        self.transform.translation = translation
        self._add_data_to_estimator()

        depths = self.estimator.get_depths()
        self.assertEqual(0, len(depths))

    def test_out_of_neighborhood(self):
        self.features[1].position[1] += 324
        self._add_data_to_estimator()
        self._simulate_camera_motion()
        self._add_data_to_estimator()

        depths = self.estimator.get_depths()
        self.assertEqual(0, len(depths))

    def test_get_depth_before_initialization_and_after_first_data(self):
        depths = self.estimator.get_depths()
        self.assertEqual(0, len(depths))

        self._add_data_to_estimator()
        depths = self.estimator.get_depths()
        self.assertEqual(0, len(depths))

        self._simulate_camera_motion()
        self._add_data_to_estimator()
        depths = self.estimator.get_depths()
        self.assertNotEqual(0, len(depths))

    def test_feature_lost(self):
        new_feature = Feature([10, 21], 3)
        self.features.append(new_feature)
        self._add_data_to_estimator()

        del self.features[2]
        self._simulate_camera_motion()
        self._add_data_to_estimator()

        depths = self.estimator.get_depths()
        f1 = self.features[0]
        f2 = self.features[1]
        self.assertTrue((f1.position == depths[0].position).all())
        self.assertTrue((f2.position == depths[1].position).all())
        self.assertEqual(3/4, depths[0].depth)
        self.assertEqual(3/4, depths[1].depth)

    def test_empty_feature_list_causes_no_exception(self):
        self._add_data_to_estimator()
        self._simulate_camera_motion()
        self.features = []
        self._add_data_to_estimator()

        depths = self.estimator.get_depths()
        self.assertEqual(0, len(depths))

    def test_first_empty_feature_list_couses_no_exception(self):
        features = self.features
        self.features = []
        self._add_data_to_estimator()

        self.features = features
        self.transform.translation += [1, 1, 0]
        self._add_data_to_estimator()

        self._simulate_camera_motion()
        self._add_data_to_estimator()

        depths = self.estimator.get_depths()
        self.assertNotEqual(0, len(depths))

    def test_feature_convergence_produce_no_depths(self):
        self.features = [Feature([10, 20], 1),
                         Feature([10, 24], 2),
                         Feature([14, 22], 3),
                         Feature([15, 20], 4),
                         Feature([12, 16], 5),
                         Feature([8, 16], 6),
                         Feature([6, 20], 7),
                         Feature([8, 24], 8)]
        self._add_data_to_estimator()

        self.features[1].position = [10, 22]
        self.features[2].position = [12, 21]
        self.features[3].position = [13, 20]
        self.features[4].position = [11, 18]
        self.features[5].position = [9, 18]
        self.features[6].position = [8, 20]
        self.features[7].position = [9, 22]
        self._add_data_to_estimator()

        depths = self.estimator.get_depths()
        self.assertEqual(0, len(depths))

    def _add_data_to_estimator(self):
        self.estimator.add_features_and_camera_pose(self.features, self.transform)

    def _simulate_camera_motion(self):
        self.features[0].position -= [1, 2]
        self.features[1].position += [1, 2]
        self.transform.translation[0] += 1.5


if __name__ == '__main__':
    unittest.main()
