#!/usr/bin/env python

from mj_computer_vision.msg import Feature as FeatureMsg, FeatureList,\
    DepthFeature as DepthFeatureMsg, DepthFeatureList
from mj_computer_vision.feature_tracker import Feature
from mj_computer_vision.depth_estimator import DepthFeature

import numpy as np


def feature_to_msg(feature):
    msg = FeatureMsg()
    msg.position.x = feature.position[0]
    msg.position.y = feature.position[1]
    msg.id = feature.id
    return msg


def msg_to_feature(msg):
    position = [msg.position.x, msg.position.y]
    return Feature(position, msg.id)


def feature_list_to_msg(features):
    msg = FeatureList()
    for feature in features:
        feature_msg = feature_to_msg(feature)
        msg.features.append(feature_msg)

    return msg


def msg_to_feature_list(msg):
    features = []
    for feature_msg in msg.features:
        f = msg_to_feature(feature_msg)
        features.append(f)

    return features


def point_msg_to_numpy(msg):
    return np.array([msg.x, msg.y, msg.z])


def depth_feature_to_msg(feature):
    msg = DepthFeatureMsg()
    msg.position.x = feature.position[0]
    msg.position.y = feature.position[1]
    msg.depth = feature.depth
    return msg


def msg_to_depth_feature(msg):
    position = [msg.position.x, msg.position.y]
    return DepthFeature(position, msg.depth)


def depth_feature_list_to_msg(features):
    msg = DepthFeatureList()
    for feature in features:
        feature_msg = depth_feature_to_msg(feature)
        msg.features.append(feature_msg)

    return msg


def msg_to_depth_feature_list(msg):
    features = []
    for feature_msg in msg.features:
        f = msg_to_depth_feature(feature_msg)
        features.append(f)

    return features
