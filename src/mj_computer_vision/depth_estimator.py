#!/usr/bin/env python

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import numpy as np
from numpy.linalg import norm
from pyquaternion import Quaternion
from copy import deepcopy
from abc import ABCMeta, abstractmethod


class DepthEstimator(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        self._features = {}
        self._old_features = {}

        self._common_features_old_positions = np.array([])
        self._common_features_new_positions = np.array([])

        self._camera_position = None
        self._old_camera_position = None
        self._depths = []

    def add_features_and_camera_pose(self, feature_list, camera_position):
        self._extract_data(feature_list, camera_position)

        if self._can_calculate_depth():
            self._calculate_depths()

    def _extract_data(self, feature_list, camera_position):
        self._old_camera_position = self._camera_position
        self._camera_position = deepcopy(camera_position)

        self._old_features = self._features
        self._features = {feature.id: deepcopy(feature)
                          for feature in feature_list}

        self._calculate_new_and_old_positions_for_common_features()

    def _calculate_new_and_old_positions_for_common_features(self):
        common_ids = [identity for identity in self._features
                      if identity in self._old_features]
        old_positions = []
        new_positions = []
        for identity in common_ids:
            old_positions.append(self._old_features[identity].position)
            new_positions.append(self._features[identity].position)

        self._common_features_old_positions = np.array(old_positions)
        self._common_features_new_positions = np.array(new_positions)

    def _can_calculate_depth(self):
        if self._old_camera_position is not None:
            camera_moved = (self._old_camera_position.translation
                            != self._camera_position.translation).any()
            if camera_moved:
                return True

        return False

    def _calculate_depths(self):
        self._depths = []
        common_feautre_count = len(self._common_features_new_positions)
        for i in xrange(common_feautre_count):
            self._add_depth_of_feature(i)

    def _add_depth_of_feature(self, i):
        try:
            depth = self._get_depth_of_feature(i)
            if depth >= 10 or depth <= 0:
                msg = 'estimated depth out of range'
                raise CouldNotCalculateDistanceError(msg)
            position = self._common_features_new_positions[i]
            depth_feature = DepthFeature(position, depth)
            self._depths.append(depth_feature)
        except CouldNotCalculateDistanceError:
            pass

    @abstractmethod
    def _get_depth_of_feature(self, i):
        pass

    def get_depths(self):
        return deepcopy(self._depths)


class EnlargmentDepthEstimator(DepthEstimator):
    def __init__(self, neighborhood_size=50):
        DepthEstimator.__init__(self)
        self.neighborhood_size = neighborhood_size

    def _get_depth_of_feature(self, i):
        old_distances, new_distances = \
            self._get_distances_of_relevant_features_to_feature(i)

        enlargment_ratios = new_distances / old_distances
        neighborhood_depths = (self._get_distance_travelled()
                               / (enlargment_ratios - 1))

        # Taking meadian is assumption that most features are on the
        # same object. Other options are mean of depths and mean of
        # enlargment.
        return np.median(neighborhood_depths)

    def _get_distances_of_relevant_features_to_feature(self, i):
        relative_old_positions = (self._common_features_old_positions
                                  - self._common_features_old_positions[i])
        relative_new_positions = (self._common_features_new_positions
                                  - self._common_features_new_positions[i])

        old_distances = norm(relative_old_positions, axis=1)
        new_distances = norm(relative_new_positions, axis=1)

        mask = self._get_relevant_features_mask(old_distances, new_distances)
        if not mask.any():
            raise CouldNotCalculateDistanceError('No features in neighborhood')

        old_distances = old_distances[mask]
        new_distances = new_distances[mask]

        return old_distances, new_distances

    def _get_relevant_features_mask(self, old_distances, new_distances):
        divergence_mask = new_distances > old_distances
        neighborhood_mask = old_distances < self.neighborhood_size
        return divergence_mask & neighborhood_mask

    def _get_distance_travelled(self):
        translation = (self._camera_position.translation
                       - self._old_camera_position.translation)
        return norm(translation)


class FullMovementDepthEstimator(DepthEstimator):
    def __init__(self, focal_x, focal_y, image_width, image_height):
        """Image size and focals should be given in pixels"""
        DepthEstimator.__init__(self)
        self._focal_x = focal_x
        self._focal_y = focal_y
        self._image_width = image_width
        self._image_height = image_height

    def _get_depth_of_feature(self, i):
        # TODO: split this method into several methods
        # TODO: camera positions should be passed into this class in proper
        #       form i.e. OZ prependicular to image plane, OY down, OX right.
        #       Need to move this conversion into ROS node.
        x_rotation = Quaternion(axis=(1, 0, 0), degrees=-90)
        z_rotation = Quaternion(axis=(0, 0, 1), degrees=-90)
        rotation = z_rotation * x_rotation

        p = deepcopy(self._camera_position)
        po = deepcopy(self._old_camera_position)

        p.rotation = p.rotation * rotation
        po.rotation = po.rotation * rotation
        transform = po.get_transform_to(p)

        new_pixel_coords = self._common_features_new_positions[i]
        new_pixel_coords = new_pixel_coords.astype(np.float)
        old_pixel_coords = self._common_features_old_positions[i]
        old_pixel_coords = old_pixel_coords.astype(np.float)

        # get film coords
        image_size = np.array([self._image_width, self._image_height])
        new_film_coords = (new_pixel_coords - image_size / 2)
        new_film_coords /= np.array([self._focal_x, self._focal_y])
        new_film_coords = np.concatenate((new_film_coords, [1]))

        old_film_coords = (old_pixel_coords - image_size / 2)
        old_film_coords /= np.array([self._focal_x, self._focal_y])
        old_film_coords = np.concatenate((old_film_coords, [1]))

        # calculate r
        rotation_matrix = transform.rotation.rotation_matrix
        translation = transform.translation
        rx = np.sum(rotation_matrix[0] * old_film_coords)
        ry = np.sum(rotation_matrix[1] * old_film_coords)
        rz = np.sum(rotation_matrix[2] * old_film_coords)
        tx, ty, tz = translation
        x2, y2 = new_film_coords[:2]
        wx = rx - rz * x2
        wy = ry - rz * y2

        if abs(wx) + abs(wy) == 0:
            raise CouldNotCalculateDistanceError('division by 0')

        Z2_x = (rx * tz - rz * tx) * np.sign(wx)
        Z2_y = (ry * tz - rz * ty) * np.sign(wy)
        Z2 = (Z2_x + Z2_y) / (abs(wx) + abs(wy))

        return Z2


class EnlargmentRelativeToCameraCenterDepthEstimator(DepthEstimator):
    def __init__(self, image_width, image_height, focal_y):
        DepthEstimator.__init__(self)
        self._image_width = image_width
        self._image_height = image_height
        self._focal_y = focal_y

    def _get_depth_of_feature(self, i):
        # TODO: refactor this function
        pitch_new = deepcopy(self._camera_position.rotation)
        pitch_new[1] = 0
        pitch_new[3] = 0
        pitch_new = pitch_new.normalised
        pitch_new = pitch_new.angle * np.sign(pitch_new[0] * pitch_new[2])
        pitch_new_pixels = np.tan(pitch_new) * self._focal_y
        new_position = self._common_features_new_positions[i].copy()
        new_position[1] += pitch_new_pixels

        pitch_old = deepcopy(self._old_camera_position.rotation)
        pitch_old[1] = 0
        pitch_old[3] = 0
        pitch_old = pitch_old.normalised
        pitch_old = pitch_old.angle * np.sign(pitch_old[0] * pitch_old[2])
        pitch_old_pixels = np.tan(pitch_old) * self._focal_y
        old_position = self._common_features_old_positions[i].copy()
        old_position[1] += pitch_old_pixels

        central_point = np.array([self._image_width / 2,
                                  self._image_height / 2])
        old_size = norm(old_position - central_point)
        new_size = norm(new_position - central_point)
        ratio = new_size / old_size
        if np.isclose(ratio, 1) or ratio < 1:
            raise CouldNotCalculateDistanceError('no enlargment')

        depth = self._get_distance_travelled() / (ratio - 1)
        return depth

    def _get_distance_travelled(self):
        translation = (self._camera_position.translation
                       - self._old_camera_position.translation)
        return norm(translation)


class DepthFeature(object):
    def __init__(self, position, depth):
        self.position = position
        self.depth = depth

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, value):
        self._position = np.array(value, dtype=np.float)


class CouldNotCalculateDistanceError(Exception):
    pass
