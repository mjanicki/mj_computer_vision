#!/usr/bin/env python

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import cv2
import numpy as np
from scipy.spatial.distance import cdist

from itertools import compress
from copy import deepcopy


class FeatureTracker(object):
    def __init__(self, max_count=100, feature_size=15, min_distance=30,
                 feature_quality=0.3):
        self.max_count = max_count
        self.feature_size = feature_size
        self.min_distance = min_distance
        self.feature_quality = feature_quality

        self._current_image = None
        self._old_image = None
        self._current_points = self._empty_point_list_factory()
        self._old_points = self._empty_point_list_factory()
        self._next_feature_id = 1
        self._features = []

    def _empty_point_list_factory(self):
        return np.array([]).reshape(-1, 2).astype(np.float32)

    def get_features(self):
        return deepcopy(self._features)

    def add_image(self, grey_image):
        self._current_image = grey_image

        if self._history_data_is_valid():
            self._update_features_with_current_image()
        self._find_new_features()

        self._cleanup_for_next_call()

    def _history_data_is_valid(self):
        return (self._old_image is not None and
                len(self._old_points) > 0)

    def _update_features_with_current_image(self):
        points_status = self._find_points_locations_on_current_image()
        self._remove_not_found_points(points_status)
        self._apply_minimal_distance()
        self._update_features_with_new_locations()

    def _find_points_locations_on_current_image(self):
        win_size = (self.feature_size, self.feature_size)
        points, status, error = cv2.calcOpticalFlowPyrLK(self._old_image,
                                                         self._current_image,
                                                         self._old_points,
                                                         None,
                                                         winSize=win_size)
        status = status.reshape(-1)
        points = points

        # Sometimes points got negative values, or grater than image size
        # I do not know what causes that.
        max_x = self._old_image.shape[1] - 1
        max_y = self._old_image.shape[0] - 1
        status &= (points[:, 0] > 0).astype(status.dtype)
        status &= (points[:, 1] > 0).astype(status.dtype)
        status &= (points[:, 0] < max_x).astype(status.dtype)
        status &= (points[:, 1] < max_y).astype(status.dtype)

        self._current_points = points
        return status

    def _remove_not_found_points(self, status):
        self._current_points = self._current_points[status == 1]
        self._features = list(compress(self._features, status))

    def _apply_minimal_distance(self):
        points_count = len(self._current_points)
        distance_matrix = cdist(self._current_points, self._current_points)
        points_to_remove = np.zeros(points_count, dtype=np.bool)

        for i in xrange(len(self._current_points)):
            range_mask = np.ones(points_count, dtype=np.bool)
            range_mask[:i + 1] = False
            distances = distance_matrix[i][range_mask]
            points_to_remove[range_mask] |= distances < self.min_distance / 2

        mask = np.invert(points_to_remove)
        self._current_points = self._current_points[mask]
        self._features = list(compress(self._features, mask))

    def _update_features_with_new_locations(self):
        for i in xrange(len(self._features)):
            self._features[i].position = self._current_points[i]

    def _find_new_features(self):
        limit = self.max_count - len(self._current_points)
        if limit <= 0:
            return

        mask = self._get_mask_for_new_features()
        points = cv2.goodFeaturesToTrack(self._current_image, mask=mask,
                                         maxCorners=limit,
                                         qualityLevel=self.feature_quality,
                                         minDistance=self.min_distance,
                                         blockSize=self.feature_size)
        if points is None:
            points = self._empty_point_list_factory()
        points = points.reshape(-1, 2)

        self._add_new_features(points)

    def _get_mask_for_new_features(self):
        mask = np.ones_like(self._current_image)
        for point in self._current_points:
            point = tuple(point)
            mask = cv2.circle(mask, point, self.min_distance, 0, -1)
        return mask

    def _add_new_features(self, points_to_add):
        self._current_points = np.concatenate((self._current_points,
                                               points_to_add))
        for point in points_to_add:
            feature = Feature(point, self._next_feature_id)
            self._next_feature_id += 1
            self._features.append(feature)

    def _cleanup_for_next_call(self):
        self._old_image = self._current_image
        self._current_image = None

        self._old_points = self._current_points
        self._current_points = self._empty_point_list_factory()


class Feature(object):
    def __init__(self, position, id_number):
        self.position = position
        self.id = id_number

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, value):
        self._position = np.array(value, dtype=np.float)

    def __str__(self):
        return '{}: ({}, {})'.format(self.id, *self.position)

    def __repr__(self):
        return str(self)


def draw_current_features_on_image(tracker, image):
    green = (0, 255, 0)
    features = tracker.get_features()
    for feature in features:
        id_location = feature.position.copy()
        id_location[0] -= tracker.feature_size
        id_location = tuple(id_location.astype(np.int))
        cv2.putText(image, str(feature.id), id_location,
                    cv2.FONT_HERSHEY_PLAIN, 1, green)

        circle_location = tuple(feature.position.astype(np.int))
        cv2.circle(image, circle_location, tracker.feature_size, green)

    feature_count = len(features)
    counter_location = (10, image.shape[0] - 10)
    cv2.putText(image, str(feature_count), counter_location,
                cv2.FONT_HERSHEY_PLAIN, 2, green)


if __name__ == '__main__':
    video_capture = cv2.VideoCapture(0)
    feature_size = 15
    tracker = FeatureTracker(max_count=100, feature_size=feature_size,
                             min_distance=feature_size*2, feature_quality=0.3)
    while True:
        ret, image = video_capture.read()
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        tracker.add_image(gray_image)
        features = tracker.get_features()

        draw_current_features_on_image(tracker, image)
        cv2.imshow('features', image)

        key = cv2.waitKey(1)
        if key == 27:
            break

    video_capture.release()
    cv2.destroyAllWindows()
