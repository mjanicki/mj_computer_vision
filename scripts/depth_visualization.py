#!/usr/bin/env python

from __future__ import division, absolute_import
from __future__ import print_function

import rospy
import message_filters
from message_filters import TimeSynchronizer
from mj_computer_vision.msg import DepthFeatureList
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from mj_computer_vision.ros_bridge import msg_to_depth_feature_list

from colorsys import hsv_to_rgb
import cv2
import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt


class ROSDepthVisualizationPlugin(object):
    def __init__(self, max_depth=50, neighborhood_size=50):
        self.max_depth = max_depth
        self.neighborhood_size = neighborhood_size
        self.cv_bridge = CvBridge()

        self._image_with_depths_publisher = \
            ImageWithDepthsPublisher(neighborhood_size, max_depth)

        self._error_image_publisher = ErrorImagePublisher()

        self._true_depth_publisher = TrueFeatureDepthPublisher(max_depth)

        self._create_message_synchronizer()

    def _create_message_synchronizer(self):
        feature_sub = message_filters.Subscriber('~input_depth_features',
                                                 DepthFeatureList)
        image_sub = message_filters.Subscriber('~input_image',
                                               Image)
        depth_sub = message_filters.Subscriber('~input_depth_image',
                                               Image)
        self.sync_subscriber = TimeSynchronizer(
            [feature_sub, image_sub, depth_sub], 70)
        self.sync_subscriber.registerCallback(
            self._feature_and_transform_callback)

    def _feature_and_transform_callback(self, features_msg, image_msg,
                                        depth_image_msg):
        self._try_to_unpack_messages(features_msg, image_msg, depth_image_msg)
        if self.messages_unpacked:
            self._image_with_depths_publisher.publish(self.image,
                                                      self.features)

            self._error_image_publisher.publish(self.image, self.depth_image,
                                                self.features)

            self._true_depth_publisher.publish(self.image, self.depth_image,
                                               self.features)

    def _try_to_unpack_messages(self, features_msg, image_msg,
                                depth_image_msg):
        self.features = msg_to_depth_feature_list(features_msg)
        try:
            self.image = self.cv_bridge.imgmsg_to_cv2(image_msg, 'bgr8')
            self.depth_image = self.cv_bridge.imgmsg_to_cv2(depth_image_msg)
            self.messages_unpacked = True
        except CvBridgeError as e:
            rospy.logerr(e)
            self.messages_unpacked = False
            return

    def spin(self):
        rospy.spin()
        self._error_image_publisher.save_histogram()


class ImagePublisher(object):
    def __init__(self, topic_name):
        self._cv_bridge = CvBridge()
        self._ros_publisher = rospy.Publisher(topic_name, Image, queue_size=0)

    def publish(self, image, *args):
        self._image = image.copy()
        self._decorate_image(*args)

        msg = self._cv_bridge.cv2_to_imgmsg(self._image)
        self._ros_publisher.publish(msg)

    def _decorate_image(self, *args):
        pass


class ImageWithDepthsPublisher(ImagePublisher):
    def __init__(self, neighborhood_size, max_depth):
        ImagePublisher.__init__(self, '~output_image')
        self._neighborhood_size = neighborhood_size
        self._max_depth = max_depth

    def _decorate_image(self, features):
        self._features = features
        self._draw_depths()
        self._draw_neighborhood_size()
        self._draw_depth_scale()

    def _draw_depths(self):
        for feature in self._features:
            color = value_to_bgr(feature.depth, upper_band=self._max_depth)
            position = tuple(feature.position.astype(np.int))
            cv2.circle(self._image, position, 5, color, -1)

    def _draw_neighborhood_size(self):
        image_height = self._image.shape[0]
        position = (neighborhood_size, image_height - neighborhood_size)
        cv2.circle(self._image, position, self._neighborhood_size,
                   (0, 255, 0), 1)

    def _draw_depth_scale(self):
        self._image = draw_rgb_scale(self._image, '0', str(self._max_depth))


class ErrorImagePublisher(ImagePublisher):
    def __init__(self):
        ImagePublisher.__init__(self, '~output_error')
        self._error_calculator = ErrorCalculator()

    def _decorate_image(self, true_depth_image, features):
        self._image = np.zeros_like(self._image)
        self._true_depth_image = true_depth_image

        for feature in features:
            self._add_feature_error(feature)
        self._draw_scale()
        self._draw_title()

        mean, std_dev, count = self._error_calculator.get_stats()
        log = '\n{}\n'\
              'mean error: {:.3f}\n'\
              'standard deviation: {:.3f}\n'\
              'number of measurements: {}\n'.format(
                  rospy.get_name(), mean, std_dev, count)
        rospy.loginfo(log)

    def _add_feature_error(self, feature):
        position = tuple(feature.position.astype(np.int))
        camera_depth = self._true_depth_image[tuple(reversed(position))]
        if np.isnan(camera_depth):
            rospy.logerr('NaN true depth')
            return

        error = self._error_calculator.add_next_measurement(feature.depth,
                                                            camera_depth)
        error = abs(error)
        color = value_to_bgr(error, upper_band=2)
        cv2.circle(self._image, position, 5, color, -1)

    def _draw_scale(self):
        self._image = draw_rgb_scale(self._image, 'x0', 'x2')

    def _draw_title(self):
        cv2.putText(self._image, rospy.get_name(), (0, 20),
                    cv2.FONT_HERSHEY_PLAIN, 1.4, (0, 255, 0), thickness=2)

    def save_histogram(self):
        file_name = rospy.get_name().replace('/', '_')
        file_name = '/tmp/' + file_name + '.png'
        self._error_calculator.save_histogram(file_name)


class TrueFeatureDepthPublisher(ImagePublisher):
    def __init__(self, max_depth):
        ImagePublisher.__init__(self, '~output_true_depth')
        self._max_depth = max_depth

    def _decorate_image(self, true_depth_image, features):
        self._true_depth_image = true_depth_image
        self._features = features

        self._draw_title()
        self._draw_features()
        self._draw_scale()

    def _draw_title(self):
        cv2.putText(self._image, 'True depth', (0, self._image.shape[0]),
                    cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))

    def _draw_features(self):
        for feature in self._features:
            position = tuple(feature.position.astype(np.int))
            camera_depth = self._true_depth_image[tuple(reversed(position))]
            if not np.isnan(camera_depth):
                color = value_to_bgr(camera_depth, upper_band=self._max_depth)
                cv2.circle(self._image, position, 5, color, -1)

    def _draw_scale(self):
        self._image = draw_rgb_scale(self._image, '0', str(self._max_depth))


class ErrorCalculator(object):
    def __init__(self):
        self._errors = []

    def get_stats(self):
        if len(self._errors) == 0:
            return 0, 0, 0
        errors = np.array(self._errors)
        return np.mean(errors), np.std(errors), errors.shape[0]

    def add_next_measurement(self, measurement, true_value):
        error = (measurement - true_value) / true_value
        self._errors.append(error)
        return error

    def save_histogram(self, file_name='histogram.png'):
        if len(self._errors) == 0:
            return

        mean, std, count = self.get_stats()
        fig, ax = plt.subplots(num=file_name, clear=True)
        n, bins, patches = ax.hist(self._errors, bins='auto', density=True)

        ax.set_ylabel('probability density')
        ax.set_title('$\mu = {:.3f}$, $\sigma = {:.3f}$, $n = {}$'.format(
            mean, std, count))
        fig.tight_layout()
        fig.savefig(file_name)


def value_to_bgr(value, lower_band=0, upper_band=1):
    value = min(value, upper_band)
    value = max(value, lower_band)

    hue = value / upper_band * 2 / 3
    rgb = hsv_to_rgb(hue, 1, 1)
    bgr = (rgb[2] * 255, rgb[1] * 255, rgb[0] * 255)
    return bgr


def draw_rgb_scale(oryginal_image, red_label='', blue_label=''):
    image = oryginal_image.copy()
    height = image.shape[0]
    scale = np.zeros((height, 30, 3), dtype=np.uint8)
    for i in xrange(height):
        value = i / (height - 1)
        color = value_to_bgr(value)
        scale[height - i - 1, :] = color

    image = np.concatenate((image, scale), axis=1)

    width = image.shape[1]
    min_value_position = (width - 30, height)
    cv2.putText(image, red_label, min_value_position,
                cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))

    max_value_position = (width - 30, 10)
    cv2.putText(image, blue_label, max_value_position,
                cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))

    return image


if __name__ == '__main__':
    rospy.init_node('depth_visualization', anonymous=True)
    max_depth = rospy.get_param('~max_depth', 50)
    neighborhood_size = rospy.get_param('~neighborhood_size', 0)

    ros_plugin = ROSDepthVisualizationPlugin(max_depth, neighborhood_size)
    ros_plugin.spin()
