#!/usr/bin/env python

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import rospy
import message_filters
from message_filters import TimeSynchronizer
from mj_computer_vision.msg import DepthFeatureList, FeatureList
from geometry_msgs.msg import TransformStamped

from mj_computer_vision.ros_bridge import msg_to_feature_list,\
    depth_feature_list_to_msg
from mj_utils.ros_messages_bridge import TransformBridge

from numpy.linalg import norm


class ROSDepthEstimationPlugin(object):
    def __init__(self, depth_estimator):
        self.depth_estimator = depth_estimator

        self.depth_feature_publisher = rospy.Publisher('~depth_features',
                                                       DepthFeatureList,
                                                       queue_size=0)
        self._create_visual_feature_and_transform_synchronizer()
        self._old_transform = None

    def _create_visual_feature_and_transform_synchronizer(self):
        feature_sub = message_filters.Subscriber('~input_visual_features',
                                                 FeatureList)
        position_sub = message_filters.Subscriber('~input_transform',
                                                  TransformStamped)
        self.sync_subscriber = TimeSynchronizer(
            [feature_sub, position_sub], 70)
        self.sync_subscriber.registerCallback(
            self._feature_and_transform_callback)

    def _feature_and_transform_callback(self, features_msg,
                                        transform_stamped_msg):
        features = msg_to_feature_list(features_msg)
        transform = TransformBridge.from_msg(transform_stamped_msg.transform)
        if self._movement_below_threshold(transform):
            return

        self.depth_estimator.add_features_and_camera_pose(features, transform)
        depths = self.depth_estimator.get_depths()

        depths_msg = depth_feature_list_to_msg(depths)
        depths_msg.header = features_msg.header
        self.depth_feature_publisher.publish(depths_msg)

        self._old_transform = transform

    def _movement_below_threshold(self, new_transform):
        if self._old_transform is None:
            self._old_transform = new_transform
            return False
        else:
            length = norm(new_transform.translation
                          - self._old_transform.translation)
            return length < .01

    def spin(self):
        rospy.spin()
