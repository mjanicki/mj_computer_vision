#!/usr/bin/env python

from __future__ import division, absolute_import
from __future__ import print_function

import rospy
from mj_computer_vision.msg import FeatureList
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

from mj_computer_vision.feature_tracker import FeatureTracker,\
    draw_current_features_on_image
from mj_computer_vision.ros_bridge import feature_list_to_msg


class ROSFeaturePlugin(object):
    def __init__(self, feature_tracker):
        self.tracker = feature_tracker
        self.cv_bridge = CvBridge()
        self.feature_publisher = rospy.Publisher('~features', FeatureList,
                                                 queue_size=0)
        self.debug_image_publisher = rospy.Publisher('~debug_image', Image,
                                                     queue_size=0)
        self.subscriber = rospy.Subscriber('~input_image', Image,
                                           self._image_callback)

    def _image_callback(self, msg):
        self._image_msg = msg
        self._try_to_unpack_image()
        if self._image_unpacked:
            self._calculate_features()
            self._publish_features()
            self._publish_debug_image()

    def _try_to_unpack_image(self):
        try:
            self._image = self.cv_bridge.imgmsg_to_cv2(self._image_msg,
                                                       'mono8')
            self._image_unpacked = True
        except CvBridgeError as e:
            rospy.logerr(e)
            self._image_unpacked = False
            return

    def _calculate_features(self):
        self.tracker.add_image(self._image)
        self._features = self.tracker.get_features()

    def _publish_features(self):
        feature_list_msg = feature_list_to_msg(self._features)
        feature_list_msg.header = self._image_msg.header
        self.feature_publisher.publish(feature_list_msg)

    def _publish_debug_image(self):
        if self.debug_image_publisher.get_num_connections() > 0:
            debug_image = self.cv_bridge.imgmsg_to_cv2(self._image_msg, 'bgr8')
            draw_current_features_on_image(self.tracker, debug_image)
            debug_msg = self.cv_bridge.cv2_to_imgmsg(debug_image)
            self.debug_image_publisher.publish(debug_msg)

    def spin(self):
        rospy.spin()


if __name__ == '__main__':
    rospy.init_node('features', anonymous=True)
    max_count = rospy.get_param('~max_count', 100)
    feature_size = rospy.get_param('~feature_size', 15)
    min_distance = rospy.get_param('~min_distance', 30)
    quality = rospy.get_param('~quality', 0.3)

    tracker = FeatureTracker(max_count, feature_size, min_distance, quality)
    ros_plugin = ROSFeaturePlugin(tracker)
    ros_plugin.spin()
