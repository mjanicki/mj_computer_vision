#!/usr/bin/env python

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import rospy
from _ros_depth_estimation_plugin import ROSDepthEstimationPlugin
from mj_computer_vision.depth_estimator import FullMovementDepthEstimator

if __name__ == '__main__':
    rospy.init_node('depth_estimation', anonymous=True)

    estimator = FullMovementDepthEstimator(554.382712822644, 554.382712822644,
                                           640, 480)
    ros_plugin = ROSDepthEstimationPlugin(estimator)
    ros_plugin.spin()
