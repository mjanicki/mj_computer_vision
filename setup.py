from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['mj_computer_vision'],
    package_dir={'': 'src'}
)

setup(**d)
